<?php

namespace Ostendis\Auth\models\plain;

use yii\base\Model;

/**
 * Class LoginCredentials
 *
 * @package   Ostendis\Auth\models\plain
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class LoginCredentials extends Model
{
    const SCENARIO_CLASSIC_LOGIN = 'classic_login';
    const SCENARIO_HASH_LOGIN = 'hash_login';
    const SCENARIO_OTP_LOGIN = 'otp_login';

    /** @var string */
    public $hash;

    /** @var string */
    public $otp;

    /** @var string */
    public $email;

    /** @var string */
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hash', 'email'], 'trim'],
            [['hash', 'email', 'password'], 'default'],
            [['hash', 'email', 'password'], 'safe'],
            [['email', 'password'], 'required', 'on' => self::SCENARIO_CLASSIC_LOGIN],
            [['hash'], 'required', 'on' => self::SCENARIO_HASH_LOGIN],
            [['otp'], 'required', 'on' => self::SCENARIO_OTP_LOGIN],

            [['email'], 'email'],
            [['hash'], 'string', 'length' => 64],
            [['otp'], 'string', 'length' => 16],
            [['email'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 500],
        ];
    }

}
