<?php
$params = require __DIR__ . '/test_params.php';
$db = require __DIR__ . '/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id'         => 'yii2-auth',
    'basePath'   => dirname(__DIR__),
    'language'   => 'de',
    'timeZone'   => 'Europe/Zurich',
    'components' => [
        'formatter'    => [
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'dateFormat'     => 'php:Y-m-d',
            'timeFormat'     => 'php:H:i:s',
        ],
        'db' => $db,
    ],

    'modules' => [
    ],

    'params' => $params,
];
