<?php

namespace Ostendis\Auth\models;

/**
 * Class User
 *
 * @package   Ostendis\Auth\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $language;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        // method is not needed in case of pure stateless RESTful application
        // see https://www.yiiframework.com/doc/guide/2.0/en/security-authentication#implementing-identity
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = [
            'id'       => $token->getClaim('id'),
            'language' => $token->getClaim('language'),
        ];

        return new static($user);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        // method is not needed in case of pure stateless RESTful application
        // see https://www.yiiframework.com/doc/guide/2.0/en/security-authentication#implementing-identity
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        // method is not needed in case of pure stateless RESTful application
        // see https://www.yiiframework.com/doc/guide/2.0/en/security-authentication#implementing-identity
    }

}
