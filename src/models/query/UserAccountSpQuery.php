<?php

namespace Ostendis\Auth\models\query;

use Ostendis\StoredProcedure\StoredProcedureQuery;

/**
 * This is the StoredProcedureQuery class for [[\Ostendis\Auth\models\UserAccount]].
 *
 * @see \Ostendis\Auth\models\UserAccount
 */
class UserAccountSpQuery extends StoredProcedureQuery
{

    /**
     * @inheritdoc
     */
    public function params(array $params): StoredProcedureQuery
    {
        parent::params($params);

        return $this;
    }

}
