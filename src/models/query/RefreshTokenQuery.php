<?php

namespace Ostendis\Auth\models\query;

use DateTime;
use Exception;
use Ostendis\Utilities\helpers\DateTimeHelper;
use Ostendis\Utilities\models\enum\MsSqlData;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[\Ostendis\Auth\models\RefreshToken]].
 *
 * @see       \Ostendis\Auth\models\RefreshToken
 * @package   Ostendis\Auth\models\query
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class RefreshTokenQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\RefreshToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\RefreshToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\query\RefreshTokenQuery
     */
    public function byId(int $id)
    {
        if (!is_int($id)) {
            throw new InvalidArgumentException("Non-numeric property `id` of Model `{$this->modelClass}` can not be queried.");
        }
        return $this->where(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\query\RefreshTokenQuery
     */
    public function byToken(string $token = '')
    {
        if (empty($token)) {
            throw new InvalidArgumentException("Empty property `token` of Model `{$this->modelClass}` can not be queried.");
        }
        return $this->where(['token' => $token]);
    }

    /**
     * Find RefreshTokens that are expired until the given time.
     *
     * @param \DateTime|integer|null $expires
     * @return \Ostendis\Auth\models\query\RefreshTokenQuery
     */
    public function expired($expires = null)
    {
        try {
            $expiration = $expires;
            if ($expires === null) {
                $expiration = new DateTime();
            } else if (is_integer($expires)) {
                $expiration = DateTimeHelper::fromUnixTimestamp($expires);
            }

            $expirationExpr = new Expression('TRY_CONVERT(datetime, :expiration)', [
                ':expiration' => $expiration->format(MsSqlData::FORMAT_DATETIME2),
            ]);
            return $this->where(['<', 'expiration', $expirationExpr]);

        } catch (Exception $e) {
            Yii::error($e->getMessage(), self::class);
        }

        return null;
    }
}
