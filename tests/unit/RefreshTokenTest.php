<?php

use Codeception\Test\Unit;
use Ostendis\Auth\models\RefreshToken;
use Ostendis\Utilities\helpers\DateTimeHelper;

class RefreshTokenTest extends Unit
{
    private $createId = 123;
    private $updateId = 456;
    private $testToken = '';


    public function testCreateToken()
    {
        $data = [
            'id'         => $this->createId,
            'token'      => $this->testToken,
            'issued'     => DateTimeHelper::fromUnixTimestamp(time()),
            'expiration' => DateTimeHelper::fromUnixTimestamp(time() + 300),
            'payload'    => ['data' => 'asdf'],
        ];

        $token = new RefreshToken($data);
        $result = $token->save();

        expect($result)->true();
    }


    public function testUpdateToken()
    {
        $token = RefreshToken::find()->byToken($this->testToken)->one();

        $data = [
            'issued'     => DateTimeHelper::fromUnixTimestamp(1546300861),  // 2019-01-01 01:01:01.000
            'expiration' => DateTimeHelper::fromUnixTimestamp(time() + 300),
            'payload'    => ['data' => 'Lorem ipsum dolor sit amet.'],
        ];

        $token->setAttributes($data);
        $result = $token->save();

        expect($result)->true();
    }


    protected function _before()
    {
        $this->testToken = bin2hex(openssl_random_pseudo_bytes(64));
        $issued = Yii::$app->formatter->asDatetime(new DateTime());
        $expiration = Yii::$app->formatter->asDatetime(new DateTime());
        $query = sprintf("INSERT INTO %s (id, token, issued, expiration, payload) VALUES(%d, '%s', '%s', '%s', '{}')", RefreshToken::tableName(), $this->updateId, $this->testToken, $issued, $expiration);

        Yii::$app->db->createCommand($query)->execute();
    }

    protected function _after()
    {
        Yii::$app->db->createCommand(sprintf('DELETE FROM %s WHERE id = %d', RefreshToken::tableName(), $this->createId))->execute();
        Yii::$app->db->createCommand(sprintf('DELETE FROM %s WHERE id = %d', RefreshToken::tableName(), $this->updateId))->execute();
    }
}
