<?php

namespace Ostendis\Auth\components\rest;

use Exception;
use Ostendis\Auth\models\enum\Role;
use Ostendis\Auth\models\plain\LoginCredentials;
use Ostendis\Auth\models\RefreshToken;
use Ostendis\Auth\models\UserAccount;
use Ostendis\Utilities\helpers\JwtHelper;
use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * Class LoginAction
 *
 * @package   Ostendis\Auth\components\rest
 * @copyright 2015-2020 Ostendis AG
 * @author    Ralf Zimmermann <ralf.zimmermann@ostendis.com>
 */
class LoginAction extends Action
{

    /**
     * Login action
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\UnauthorizedHttpException
     */
    public function run()
    {
        $responseData = [];
        $params = Yii::$app->request->bodyParams;
        $headers = Yii::$app->request->headers;

        $config = isset(Yii::$app->params['ostendis-yii2-auth']) ? Yii::$app->params['ostendis-yii2-auth'] : [];

        $loginCredentials = new LoginCredentials($params);
        $loginScenario = LoginCredentials::SCENARIO_CLASSIC_LOGIN;

        if (isset($params['hash'])) {
            $loginScenario = LoginCredentials::SCENARIO_HASH_LOGIN;
        } else if (isset($params['otp'])) {
            $loginScenario = LoginCredentials::SCENARIO_OTP_LOGIN;
        }

        $loginCredentials->setScenario($loginScenario);

        if (!$loginCredentials->validate()) {
            $errors = $loginCredentials->getFirstErrors();
            throw new BadRequestHttpException(array_shift($errors));
        }

        $userAccount = UserAccount::findByLoginCredentials($loginCredentials);

        if ($userAccount === null) {
            throw new UnauthorizedHttpException('The provided login data is invalid or account does not exist.');
        }

        $this->assignUserRoles($userAccount);
        $userRole = Yii::$app->authManager->getRole($userAccount->editionId);

        $payload = [
            'id'             => $userAccount->id,
            'language'       => $userAccount->language,
            'firstName'      => $userAccount->firstName,
            'lastName'       => $userAccount->lastName,
            'email'          => $userAccount->email,
            'role'           => $userRole->name,
            'roleName'       => $userRole->description,
            'isCompanyAdmin' => $userAccount->isCompanyAdmin,
            'isAdmin'        => $userAccount->isAdmin,
            'companyId'      => $userAccount->companyId,
            'companyName'    => $userAccount->companyName,
        ];

        $tokenBuilder = JwtHelper::generateToken($config['accessToken']);
        JwtHelper::setPayload($tokenBuilder, $payload);

        $responseData['accessToken'] = (string)JwtHelper::getSignedToken($tokenBuilder);
        $responseData['refreshToken'] = RefreshToken::generate($payload, $config['refreshToken']);

        return $responseData;
    }

    /**
     * Revoke and re-assign roles to a user
     *
     * @param \Ostendis\Auth\models\UserAccount $userAccount
     * @throws \Exception
     */
    protected function assignUserRoles(UserAccount $userAccount): void
    {
        /** @var \yii\rbac\PhpManager $auth */
        $auth = Yii::$app->authManager;

        if (count($auth->getRolesByUser($userAccount->id))) {
            if (!$auth->revokeAll($userAccount->id)) {
                Yii::error('Could not revoke user roles', __METHOD__);
                throw new Exception('User role assignment failed');
            }
        }

        $auth->assign($auth->getRole($userAccount->editionId), $userAccount->id);
        if ($userAccount->isCompanyAdmin) {
            $auth->assign($auth->getRole(Role::COMPANY_ADMIN), $userAccount->id);
        }
        if ($userAccount->isAdmin) {
            $auth->assign($auth->getRole(Role::ADMIN), $userAccount->id);
        }
    }
}