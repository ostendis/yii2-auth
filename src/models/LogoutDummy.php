<?php

namespace Ostendis\Auth\models;

use Ostendis\StoredProcedure\StoredProcedureRecord;

/**
 * Class LogoutDummy
 * Dummy model to make LogoutAction SP work by setting the DB connection from the outside
 *
 * @package   Ostendis\Auth\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class LogoutDummy extends StoredProcedureRecord
{

}
