<?php

namespace Ostendis\Auth\components\rest;

use Ostendis\Auth\models\RefreshToken;
use yii\base\Action;

/**
 * Class CleanupAction
 *
 * @package   Ostendis\Auth\components\rest
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class CleanupAction extends Action
{

    /**
     * Clean up expired RefreshTokens
     *
     * @return array
     */
    public function run()
    {
        RefreshToken::cleanupExpired();
        return [];
    }
}
