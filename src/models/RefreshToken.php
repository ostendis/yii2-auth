<?php

namespace Ostendis\Auth\models;

use DateTime;
use Ostendis\Auth\models\query\RefreshTokenQuery;
use Ostendis\Utilities\helpers\DateTimeHelper;
use Ostendis\Utilities\models\ActiveRecord;
use Ostendis\Utilities\validators\ArrayValidator;
use Ostendis\Utilities\validators\DateTimeCompareValidator;
use Ostendis\Utilities\validators\DateTimeValidator;
use Throwable;
use Yii;

/**
 * Class RefreshToken
 *
 * @package   Ostendis\Auth\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 *
 * @property int              $id
 * @property string           $token
 * @property string|\DateTime $issued
 * @property string|\DateTime $expiration
 * @property string|array     $payload
 */
class RefreshToken extends ActiveRecord
{
    const ISSUED_OFFSET = 0;
    const EXPIRATION_OFFSET = 300;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{OAA.tRefreshToken}}';
    }

    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\query\RefreshTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RefreshTokenQuery(get_called_class());
    }

    /**
     * Revoke (delete) token
     *
     * @param string $token
     * @return bool
     */
    public static function revoke(string $token): bool
    {
        /** @var RefreshToken $refreshToken */
        $refreshToken = RefreshToken::find()->byToken($token)->one();

        if ($refreshToken === null || $refreshToken === '') {
            return true;
        }

        try {
            $refreshToken->delete();
        } catch (Throwable $e) {
            Yii::error($e->getMessage(), self::class);
            return false;
        }

        return true;
    }

    /**
     * Delete all tokens that are expired until $expiration
     *
     * @param \DateTime|null $expiration
     * @return bool
     */
    public static function cleanupExpired(DateTime $expiration = null): bool
    {
        $tokenList = RefreshToken::find()->expired($expiration)->all();
        if (count($tokenList) === 0) return true;

        foreach ($tokenList as $token) {
            try {
                $token->delete();
            } catch (Throwable $e) {
                Yii::error($e->getMessage(), self::class);
                return false;
            }
        }

        return true;
    }

    /**
     * Generate and get token
     * Delete token if already exists
     *
     * @param array $payload
     * @param array $config
     * @return string
     * @throws \Exception
     */
    public static function generate(array $payload, array $config = []): string
    {
        $issuedOffset = isset($config['issued_offset']) ? $config['issued_offset'] : static::ISSUED_OFFSET;
        $expirationOffset = isset($config['expiration_offset']) ? $config['expiration_offset'] : static::EXPIRATION_OFFSET;

        /** @var RefreshToken $oldToken */
        $oldToken = RefreshToken::find()->byId($payload['id'])->one();

        if ($oldToken !== null) {
            try {
                $oldToken->delete();
            } catch (Throwable $e) {
                Yii::error($e->getMessage(), self::class);
                return false;
            }
        }

        $tokenData = [
            'token'      => bin2hex(openssl_random_pseudo_bytes(64)),
            'issued'     => DateTimeHelper::fromUnixTimestamp(time() + $issuedOffset),
            'expiration' => DateTimeHelper::fromUnixTimestamp(time() + $expirationOffset),
            'id'         => $payload['id'],
            'payload'    => $payload,
        ];

        $refreshToken = new RefreshToken($tokenData);
        $refreshToken->save();
        return $refreshToken->token;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token', 'issued', 'expiration', 'id', 'payload'], 'required'],
            [['issued', 'expiration'], DateTimeValidator::class],
            [['expiration'], DateTimeCompareValidator::class, 'operator' => '>', 'compareValue' => new DateTime()],
            [['token'], 'string', 'length' => 128],
            [['payload'], ArrayValidator::class],
            [['id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->issued = Yii::$app->formatter->asDatetime($this->issued);
        $this->expiration = Yii::$app->formatter->asDatetime($this->expiration);
        $this->payload = json_encode($this->payload);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->payload = $this->getPayload();
        $this->issued = $this->getIssued();
        $this->expiration = $this->getExpiration();

    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        if (is_string($this->payload)) {
            $this->payload = json_decode($this->payload, true);
        }
        return $this->payload;
    }

    /**
     * @return \DateTime|null
     */
    public function getIssued()
    {
        if (is_string($this->issued)) {
            $this->issued = DateTimeHelper::fromDatabase($this->issued);
        }
        return $this->issued;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiration()
    {
        if (is_string($this->expiration)) {
            $this->expiration = DateTimeHelper::fromDatabase($this->expiration);
        }
        return $this->expiration;
    }

}
