<?php

namespace Ostendis\Auth\components\rest;

use Ostendis\Auth\models\RefreshToken;
use Ostendis\Utilities\helpers\JwtHelper;
use Yii;
use yii\base\Action;
use yii\base\InvalidArgumentException;
use yii\web\HttpException;

/**
 * Class RefreshAction
 *
 * @package   Ostendis\Auth\components\rest
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class RefreshAction extends Action
{

    /**
     * Refresh action
     *
     * @return array
     * @throws Yii\web\HttpException
     */
    public function run()
    {
        $responseData = [];
        $config = isset(Yii::$app->params['ostendis-yii2-auth']) ? Yii::$app->params['ostendis-yii2-auth'] : [];
        $token = Yii::$app->request->getBodyParam('refreshToken');

        if ($token === null) {
            throw new HttpException(418, 'Required body param `refreshToken` not set');
        }

        try {
            $refreshToken = RefreshToken::find()->byToken($token)->one();
        } catch (InvalidArgumentException $e) {
            throw new HttpException(418, 'Body param `refreshToken` empty or invalid');
        }

        if ($refreshToken === null || !$refreshToken->validate() || $refreshToken->getPayload() === null) {
            throw new HttpException(418, 'RefreshToken does not exist or is not valid');
        }

        $tokenBuilder = JwtHelper::generateToken($config['accessToken']);
        JwtHelper::setPayload($tokenBuilder, $refreshToken->getPayload());

        $responseData['accessToken'] = (string)JwtHelper::getSignedToken($tokenBuilder);
        $responseData['refreshToken'] = $refreshToken->token;

        return $responseData;
    }
}
