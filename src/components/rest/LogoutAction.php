<?php

namespace Ostendis\Auth\components\rest;

use Ostendis\Auth\models\LogoutDummy;
use Ostendis\StoredProcedure\MsSqlStoredProcedure;
use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;

/**
 * Class LogoutAction
 *
 * @package   Ostendis\Auth\components\rest
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class LogoutAction extends Action
{


    /**
     * Logout action
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     */
    public function run()
    {
        if (count(Yii::$app->request->bodyParams)) {
            throw new BadRequestHttpException('Body params must not be set on logout');
        }

        $logoutSp = new MsSqlStoredProcedure([
            'name'  => 'OAA.setAccountLogout',
            'model' => LogoutDummy::class,
        ]);
        $logoutSp->setParams([
            'AccountID'   => Yii::$app->user->id,
            'REMOTE_ADDR' => Yii::$app->request->userIP,
        ]);
        $logoutSp->execute();

        return [];
    }
}
