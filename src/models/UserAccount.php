<?php

namespace Ostendis\Auth\models;

use Ostendis\Auth\models\plain\LoginCredentials;
use Ostendis\Auth\models\query\UserAccountSpQuery;
use Ostendis\StoredProcedure\StoredProcedureRecord;
use Ostendis\Utilities\models\enum\MsSqlData;
use Yii;
use yii\db\mssql\PDO;

/**
 * Class UserAccount
 *
 * @package   Ostendis\Auth\models
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class UserAccount extends StoredProcedureRecord
{
    /** @var integer */
    public $id;

    /** @var string */
    public $language;

    /** @var string */
    public $email;

    /** @var string */
    public $firstName;

    /** @var string */
    public $lastName;

    /** @var integer */
    public $editionId;

    /** @var boolean */
    public $isCompanyAdmin;

    /** @var boolean */
    public $isAdmin;

    /** @var integer */
    public $companyId;

    /** @var string */
    public $companyName;


    /**
     * {@inheritdoc}
     */
    public static function spLoadNames(): array
    {
        return [
            'login' => 'OAA.getAccountLogin',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function spSaveNames(): array
    {
        return [];
    }

    /**
     * Finds Account by ID
     *
     * @param \Ostendis\Auth\models\plain\LoginCredentials $loginData
     * @return \Ostendis\Auth\models\UserAccount|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public static function findByLoginCredentials(LoginCredentials $loginData)
    {
        $headerData = Yii::$app->request->headers->toArray();

        return static::find()->params([
            'login' => [
                'ODMloginHash'    => $loginData->hash,
                'AccountOTP'      => $loginData->otp,
                'AccountEmail'    => $loginData->email,
                'AccountPW'       => $loginData->password,
                'AccountLangCode' => Yii::$app->language,
                'REMOTE_ADDR'     => Yii::$app->request->userIP,
                'HTTP_USER_AGENT' => $headerData['user-agent'][0],
            ],
        ])->inParams([
            'login' => [
                'MessageID'   => [
                    'type'    => PDO::PARAM_INT,
                    'default' => 0,
                ],
                'MessageText' => [
                    'type'    => PDO::PARAM_STR,
                    'default' => 'x',
                    'length'  => 500,
                ],
            ],
        ])->one('login');
    }

    /**
     * {@inheritdoc}
     * @return \Ostendis\Auth\models\query\UserAccountSpQuery
     */
    public static function find()
    {
        return new UserAccountSpQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function getPropertyMap(): array
    {
        return [
            'AccountCompanyMasterAccount'    => 'isCompanyAdmin',
            'AccountEditionID'               => 'editionId',
            'AccountEmail'                   => 'email',
            'AccountFirstName'               => 'firstName',
            'AccountID'                      => 'id',
            'AccountLangCode'                => 'language',
            'AccountLastName'                => 'lastName',
            'AccountPermissionOstendisAdmin' => 'isAdmin',
            'CompanyID'                      => 'companyId',
            'CompanyName'                    => 'companyName',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // Basic validators
            [['email', 'firstName', 'lastName'], 'trim'],
            [['email', 'editionId', 'firstName', 'id', 'isAdmin', 'isCompanyAdmin', 'language', 'lastName'], 'safe'],
            [['email', 'firstName', 'id', 'language', 'lastName'], 'required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],

            // Primitive validators
            [['isAdmin', 'isCompanyAdmin'], 'boolean'],

            [['id'], 'integer'],
            [['editionId'], 'integer', 'max' => MsSqlData::TINYINT_MAX],

            [['language'], 'string', 'length' => 2],
            [['firstName', 'lastName'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],

            // Special validators
            [['email'], 'email'],

        ];
    }
}
