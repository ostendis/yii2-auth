<?php

namespace Ostendis\Auth\models\enum;

use yii\base\Model;

/**
 * Class Role
 *
 * @package   Ostendis\Auth\models\enum
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class Role extends Model
{
    const COMPANY_ADMIN = 'company_admin';
    const ADMIN = 'admin';

    // Based on Edition ID's from DB
    const EDITION_FREE = 10;
    const EDITION_FREE_PLUS = 15;
    const EDITION_PRO = 100;
    const EDITION_PRO_PLUS = 102;
    const EDITION_PRO_TEST = 110;
    const EDITION_PREMIUM = 200;
    const EDITION_PREMIUM_TEST = 210;


}
