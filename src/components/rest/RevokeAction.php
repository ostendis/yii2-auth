<?php

namespace Ostendis\Auth\components\rest;

use Ostendis\Auth\models\RefreshToken;
use Yii;

/**
 * Class RevokeAction
 *
 * @package   Ostendis\Auth\components\rest
 * @copyright 2015-2019 Ostendis AG
 * @author    Tom Lutzenberger <tom.lutzenberger@ostendis.com>
 */
class RevokeAction extends \yii\base\Action
{

    /**
     * Revokes an existing RefreshToken
     *
     * @return array
     */
    public function run()
    {
        $token = Yii::$app->request->bodyParams['refreshToken'];
        RefreshToken::revoke($token);
        return [];
    }
}
